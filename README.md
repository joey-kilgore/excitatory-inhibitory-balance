# excitatory-inhibitory-balance

Surrogate gradient training on networks with various excitatory and inhibitory ratios.

## Running trials
This project is meant to be run on a high performance computing system at George Washington University that utilizes a SLURM system.
To handle this, python scripts are used to generate the shell scripts corresponding to the jobs submitted.

Within the `shellScripts` directory are python scripts that can be run from the main directory to generate all shell scripts. For example:
`python3 ./shellScripts/generateShellScripts.py` which handles generating shell scripts for Fashion-MNIST training.

Each of these scripts will contain python commands that load the main scripts for training or report generators. These commands can optionally be run individually for specific trials such as:
`python3.7 scripts/excInhTraining.py -t 0 -n 1 -l 0.01 -s sparse050_example -r 0.5 -e 30 -f sparse050_example.log -w1std 0.001 -w1sparse 0.0 -w2std -1 -w2sparse -1 --debug`

## Loading data after simulations
To ensure proper data archiving, the `simLogger.py` allows for python objects to be easily pickled by combining a unique simulation tag (the `simTag`), a unique object tag (`uniqueId`) and a date-time string to make sure all objects have a unique file name to avoid overwriting any data.
These can be loaded after the fact by checking the object directory for the simulation (default `./data/obj`)

